const data = [
{
    id: 1,
    name: 'McDonald\'s (แมคโดนัลด์) - ราชดำเนิน',
    imageURL: 'https://d1sag4ddilekf6.cloudfront.net/compressed/merchants/AWh64KYIZXYdMpch2Zem/hero/f50a98adc03f4117a0d0ee9f92b5d584_1618897333597929269.png',
    type: 'Burgers, Fast Food, Fried Chicken'
},
{
    id: 2,
    name: 'หมูปลาร้าฮีโร่ - แยกคอกวัว',
    imageURL: 'https://d1sag4ddilekf6.cloudfront.net/compressed/merchants/3-CZN1HB3BFA5CGX/hero/aa56db09b8d940939bf9afb9bb67cdf8_1600222326842784224.jpeg',
    type: 'Coupon, Cooked to Order'
},
{
    id: 3,
    name: 'Texas Chicken (เท็กซัส ชิคเก้น) - เซนต์คาเบรียล',
    imageURL: 'https://d1sag4ddilekf6.cloudfront.net/compressed/merchants/3-CYNDTUMWR8C3TA/hero/ab162eac1c134c17b54a926de04fa05d_1618804202087318798.jpg',
    type: 'Fast Food, Fried Chicken'
},
{
    id: 4,
    name: 'อรทัย ซูชิวังหลัง (ORATHAI SUSHI WANG LANG) - ตรอกวังหลัง',
    imageURL: 'https://d1sag4ddilekf6.cloudfront.net/compressed/merchants/3-C2K3PAX3DFEACJ/hero/1db0faffb5134544a66a4d16c9022f1b_1617172370053012280.jpeg',
    type: 'Sushi'
},
{
    id: 5,
    name: 'อาหารตามสั่งแยกสำราญราษฏร์ - ประตูผี',
    imageURL: 'https://d1sag4ddilekf6.cloudfront.net/compressed/merchants/3-C2L1WEBXA2TWFA/hero/e057e4bea31948c2b5d3d2af317d1800_1615864473481135416.jpeg',
    type: 'Coupon, Cooked to Order'
},
{
    id: 6,
    name: 'Potato Corner (โปเตโต้ คอร์เนอร์) - ถนนข้าวสาร',
    imageURL: 'https://d1sag4ddilekf6.cloudfront.net/compressed/merchants/3-CZLJEVDYC3JFL6/hero/dc2430f425e24d9db1ef815d3e6b9c36_1618760460477991726.jpg',
    type: 'Small Bites/Snacks'
},
{
    id: 7,
    name: 'กระเพราผัดพระราม8อาหารตาสั่ง - บางขุนพรหม',
    imageURL: 'https://d1sag4ddilekf6.cloudfront.net/compressed/merchants/3-C2J3HGNCEKV1AE/hero/fb9b6999037741ee8b977dba9313962b_1617269176702611463.jpeg',
    type: 'Coupon, Cooked to Order'
},
{
    id: 8,
    name: 'KHIANG (เขียง) - ถนนพระอาทิตย์',
    imageURL: 'https://d1sag4ddilekf6.cloudfront.net/compressed/merchants/3-CZLGGETXRBV3A6/hero/ca72b23b3bf04dc0a040fa857f1702ab_1616950273607255486.jpg',
    type: 'Rice Bowls'
},
{
    id: 9,
    name: 'ข้าวหมูทอด krit - ถนนศิริพงษ์',
    imageURL: 'https://d1sag4ddilekf6.cloudfront.net/compressed/merchants/3-CZKZN8MVEUTJEA/hero/c3ab80aeebac42f59d41d3cf14a50504_1594112097107461277.jpeg',
    type: 'Coupon, Rice Bowls'
}

];


module.exports = data;